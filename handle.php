<?php
    declare(strict_types=1);

    // Включаем режим выбрасывания исключений для MySQL
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

    /**
     * Тестовое задание на PHP-разработчика
     * @author Ivan Kryukov
     */

    $response = [];

    try {
        // Получаем настройки для работы с БД
        $setting = include('.setting.php');

        // Тут проведем валидацию CSV файла
        validate($_FILES['file'] ?? []);

        // Загрузим файл на сервер
        $filepath = upload($_FILES['file']);

        // Убираем BOM последовательность
        retriveBOM($filepath);

        // Преобразуем CSV в массив
        $lines = csvToArray($filepath, $setting['separator']);

        // Подключаемся к БД
        $mysqli = new mysqli($setting['db']['host'], $setting['db']['user'], $setting['db']['pass'], $setting['db']['name']);

        // Подготовим запрос
        $stmt = $mysqli->prepare("INSERT INTO motherboards(brand, model) VALUES(?, ?)");
        $stmt->bind_param('ss', $brand, $model);

        // Запишем данные в базу
        foreach($lines as $line) {
            $brand = $line[0] ?? '';
            $model = $line[1] ?? '';

            try {
                $stmt->execute();
            } catch(\mysqli_sql_exception $exp) {
                // Если дубликат, то идем дальше
                // Можно сделать обработчик
                if($exp->getCode() === 1062) { continue; }

                throw $exp;
            }
        }

        $mysqli->close();

        $response['message'] = 'Данные из CSV добавлены в БД';
    } catch(\FileCSVException $exp) {
        $response['message'] = $exp->getMessage();
    } catch(\mysqli_sql_exception $exp) {
        if($exp->getCode() === 1045) {
            $response['message'] = "Не могу подключится к БД";
        }

        if(in_array($exp->getCode(), [1366, 1452])) {
            $response['message'] = "Ошибка при добавлении, проверьте данные";
        }
    } catch(\Exception $exp) {
        $response['message'] = $exp->getMessage();
    } finally {
        ob_clean();
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    class FileCSVException extends \Exception {}
    
    /**
     * Валидация входных данных
     *
     * @param array $file
     * @return void
     * @throws \FileCSVException
     */
    function validate($file) {
        // Проверим на наличие файла
        if(!isset($file['tmp_name'])) {
            throw new \FileCSVException('Файл не загружен');
        }

        // Проверим на наличие ошибок при загрузке
        if($file['error'] > 0) {
            throw new \FileCSVException('Возникли ошибки при загрузке файла');
        }

        // Проверим, текстовый ли это файл
        if(mime_content_type($file['tmp_name']) !== 'text/plain') {
            throw new \FileCSVException('Передан не текстовый файл');
        }
    }

    /**
     * Загружаем файл на сервер
     *
     * @param array $file
     * @return string
     * @throws \FileCSVException
     */
    function upload($file): string {
        $destination = sprintf('%s/upload/%s.csv', __DIR__, sha1(uniqid()));

        if(!move_uploaded_file($file["tmp_name"], $destination)) {
            throw new \FileCSVException('Не удалось загрузить CSV файл на сервер');
        }

        return $destination;
    }

    /**
     * Убрать BOM последовательность из строки
     *
     * @see https://www.php.net/manual/en/function.fgetcsv.php#122696
     * @param string $filepath
     * @return void
     */
    function retriveBOM($filepath) {
        // BOM as a string for comparison.
        $bomPattern = "\xef\xbb\xbf";

        // Read file from beginning.
        $filePointer = fopen($filepath, 'r');

        // Progress file pointer and get first 3 characters to compare to the BOM string.
        if (fgets($filePointer, 4) !== $bomPattern) {
            // BOM not found - rewind pointer to start of file.
            rewind($filePointer);
        }

        fclose($filePointer);
    }

    /**
     * Преоборазуем CSV в массив
     * 
     * @param string $filepath
     * @param string $separator
     * @return array
     */
    function csvToArray($filepath, $separator): array {
        $filePointer = fopen($filepath, 'r');

        $lines = [];
        while(!feof($filePointer) && ($line = fgetcsv($filePointer, 0, $separator)) !== false) {
            $lines[] = $line;
        }

        fclose($filePointer);

        return $lines;
    }